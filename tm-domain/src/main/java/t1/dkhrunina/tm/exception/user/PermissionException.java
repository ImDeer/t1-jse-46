package t1.dkhrunina.tm.exception.user;

public class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Error: you have no permission to do this");
    }

}