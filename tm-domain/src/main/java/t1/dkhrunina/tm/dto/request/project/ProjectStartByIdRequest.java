package t1.dkhrunina.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class ProjectStartByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectStartByIdRequest(@Nullable final String token, @Nullable final String id) {
        super(token);
        this.id = id;
    }

}