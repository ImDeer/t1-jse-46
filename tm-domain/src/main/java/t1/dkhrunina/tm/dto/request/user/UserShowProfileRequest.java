package t1.dkhrunina.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class UserShowProfileRequest extends AbstractUserRequest {

    public UserShowProfileRequest(@Nullable final String token) {
        super(token);
    }

}