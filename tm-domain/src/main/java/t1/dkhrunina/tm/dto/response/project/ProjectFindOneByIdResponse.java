package t1.dkhrunina.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;
import t1.dkhrunina.tm.dto.model.ProjectDTO;

@Getter
@Setter
@NoArgsConstructor
public class ProjectFindOneByIdResponse extends AbstractResultResponse {

    @Nullable
    private ProjectDTO project;

    public ProjectFindOneByIdResponse(@Nullable final ProjectDTO project) {
        this.project = project;
    }

    public ProjectFindOneByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}