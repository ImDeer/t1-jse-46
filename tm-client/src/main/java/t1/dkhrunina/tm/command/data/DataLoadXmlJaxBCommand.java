package t1.dkhrunina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.data.DataLoadXmlJaxBRequest;

public final class DataLoadXmlJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-l-xml-jaxb";

    @NotNull
    private static final String DESCRIPTION = "Load data from JaxB XML file.";

    @SneakyThrows
    @Override
    public void execute() {
        getDomainEndpoint().loadXmlJaxBData(new DataLoadXmlJaxBRequest(getToken()));
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}