package t1.dkhrunina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.data.DataLoadJsonFasterXmlRequest;

public final class DataLoadJsonFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-l-json-faster";

    @NotNull
    private static final String DESCRIPTION = "Load data from FasterXML JSON file.";

    @SneakyThrows
    @Override
    public void execute() {
        getDomainEndpoint().loadJsonFasterXmlData(new DataLoadJsonFasterXmlRequest(getToken()));
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}