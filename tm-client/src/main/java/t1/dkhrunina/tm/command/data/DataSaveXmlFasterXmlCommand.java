package t1.dkhrunina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.data.DataSaveXmlFasterXmlRequest;

public final class DataSaveXmlFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-s-xml-faster";

    @NotNull
    private static final String DESCRIPTION = "Save data to FasterXML XML file.";

    @SneakyThrows
    @Override
    public void execute() {
        getDomainEndpoint().saveXmlFasterXmlData(new DataSaveXmlFasterXmlRequest(getToken()));
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}