package t1.dkhrunina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.user.UserChangePasswordRequest;
import t1.dkhrunina.tm.dto.response.user.UserChangePasswordResponse;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "u-change-pwd";

    @NotNull
    private static final String DESCRIPTION = "Change current user password.";

    @Override
    public void execute() {
        System.out.println("[Change user password]");
        System.out.println("Enter new password: ");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken(), password);
        @NotNull final UserChangePasswordResponse response = getUserEndpoint().changeUserPassword(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}