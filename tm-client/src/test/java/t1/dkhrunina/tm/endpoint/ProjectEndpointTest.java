package t1.dkhrunina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import t1.dkhrunina.tm.api.endpoint.IAuthEndpoint;
import t1.dkhrunina.tm.api.endpoint.IProjectEndpoint;
import t1.dkhrunina.tm.api.endpoint.IUserEndpoint;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.dto.request.project.*;
import t1.dkhrunina.tm.dto.request.user.UserLoginRequest;
import t1.dkhrunina.tm.dto.request.user.UserRegisterRequest;
import t1.dkhrunina.tm.dto.request.user.UserRemoveRequest;
import t1.dkhrunina.tm.dto.response.project.ProjectCreateResponse;
import t1.dkhrunina.tm.dto.response.project.ProjectListResponse;
import t1.dkhrunina.tm.dto.response.user.UserLoginResponse;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.marker.SoapCategory;
import t1.dkhrunina.tm.dto.model.ProjectDTO;
import t1.dkhrunina.tm.service.PropertyService;

import java.util.List;

@Category(SoapCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private static IUserEndpoint userEndpoint;

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @NotNull
    private static IProjectEndpoint projectEndpoint;

    @NotNull
    private String firstProjectId;

    @NotNull
    private String secondProjectId;

    @BeforeClass
    public static void initUser() {
        @NotNull IPropertyService propertyService = new PropertyService();
        projectEndpoint = IProjectEndpoint.newInstance(propertyService);
        @NotNull final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);
        userEndpoint = IUserEndpoint.newInstance(propertyService);
        @NotNull UserLoginRequest loginRequest = new UserLoginRequest("admin", "admin");
        @NotNull UserLoginResponse response = authEndpoint.loginUser(loginRequest);
        adminToken = response.getToken();

        @NotNull final UserRegisterRequest registerRequest = new UserRegisterRequest("test", "test", "test@test.test");
        userEndpoint.registerUser(registerRequest);
        loginRequest = new UserLoginRequest("test", "test");
        response = authEndpoint.loginUser(loginRequest);
        userToken = response.getToken();
    }

    @AfterClass
    public static void dropUser() {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken, "test");
        userEndpoint.removeUser(removeRequest);
    }

    @Before
    public void initData() {
        @NotNull ProjectCreateRequest createRequest = new ProjectCreateRequest(userToken, "testpr1", "prdesc1");
        firstProjectId = projectEndpoint.createProject(createRequest).getProject().getId();
        createRequest = new ProjectCreateRequest(userToken, "testpr2", "prdesc2");
        secondProjectId = projectEndpoint.createProject(createRequest).getProject().getId();
    }

    @After
    public void dropData() {
        @NotNull final ProjectClearRequest clearRequest = new ProjectClearRequest(userToken);
        projectEndpoint.clearProject(clearRequest);
    }

    @Nullable
    private ProjectDTO findProjectById(@NotNull final String projectId) {
        @NotNull final ProjectFindOneByIdRequest request = new ProjectFindOneByIdRequest(userToken, projectId);
        return projectEndpoint.findOneById(request).getProject();
    }

    @Nullable
    private ProjectDTO findProjectByIndex(@NotNull final Integer index) {
        @NotNull final ProjectFindOneByIndexRequest request = new ProjectFindOneByIndexRequest(userToken, index);
        return projectEndpoint.findOneByIndex(request).getProject();
    }

    @Test
    public void testChangeStatusById() {
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(userToken, firstProjectId, status);
        Assert.assertNotNull(projectEndpoint.changeProjectStatusById(request));
        @Nullable final ProjectDTO project = findProjectById(firstProjectId);
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void testChangeStatusByIdTokenNull() {
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(null, firstProjectId, Status.IN_PROGRESS);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusById(request));
    }

    @Test
    public void testChangeStatusByIndex() {
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(userToken, 1, status);
        Assert.assertNotNull(projectEndpoint.changeProjectStatusByIndex(request));
        @Nullable final ProjectDTO project = findProjectByIndex(1);
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void testChangeStatusByIndexTokenNull() {
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(null, 1, Status.IN_PROGRESS);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusByIndex(request));
    }

    @Test
    public void testClear() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(userToken);
        Assert.assertNotNull(projectEndpoint.clearProject(request));
        @NotNull final ProjectListRequest listRequest = new ProjectListRequest(userToken, null);
        @Nullable final List<ProjectDTO> projects = projectEndpoint.listProject(listRequest).getProjects();
        Assert.assertNull(projects);
    }

    @Test
    public void testClearNull() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(null);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.clearProject(request));
    }

    @Test
    public void testCompleteById() {
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(userToken, secondProjectId);
        Assert.assertNotNull(projectEndpoint.completeProjectById(request));
        @Nullable final ProjectDTO project = findProjectById(secondProjectId);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void testCompleteByIdTokenNull() {
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(null, secondProjectId);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.completeProjectById(request));
    }

    @Test
    public void testCompleteByIndex() {
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(userToken, 1);
        Assert.assertNotNull(projectEndpoint.completeProjectByIndex(request));
        @Nullable final ProjectDTO project = findProjectByIndex(1);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void testCompleteByIndexTokenNull() {
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(null, 1);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.completeProjectByIndex(request));
    }

    @Test
    public void testCreate() {
        @NotNull final String name = "NAME TEST CREATE";
        @NotNull final String desc = "DESC TEST CREATE";
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(userToken, name, desc);
        @Nullable final ProjectCreateResponse response = projectEndpoint.createProject(request);
        Assert.assertNotNull(response);
        @Nullable final ProjectDTO returnedProject = response.getProject();
        Assert.assertNotNull(returnedProject);
        @NotNull final String projectId = returnedProject.getId();
        @Nullable final ProjectDTO createdProject = findProjectById(projectId);
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(name, createdProject.getName());
        Assert.assertEquals(desc, createdProject.getDescription());
    }

    @Test
    public void testCreateTokenNull() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(null, "NAME", "DESC");
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(request));
    }

    @Test
    public void testFindOneById() {
        @NotNull final ProjectFindOneByIdRequest request = new ProjectFindOneByIdRequest(userToken, firstProjectId);
        @Nullable final ProjectDTO project = projectEndpoint.findOneById(request).getProject();
        Assert.assertNotNull(project);
    }

    @Test
    public void testFindOneByIdTokenNull() {
        @NotNull final ProjectFindOneByIdRequest request = new ProjectFindOneByIdRequest(null, firstProjectId);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.findOneById(request));
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull final ProjectFindOneByIndexRequest request = new ProjectFindOneByIndexRequest(userToken, 1);
        @Nullable final ProjectDTO project = projectEndpoint.findOneByIndex(request).getProject();
        Assert.assertNotNull(project);
    }

    @Test
    public void testFindOneByIndexTokenNull() {
        @NotNull final ProjectFindOneByIndexRequest request = new ProjectFindOneByIndexRequest(null, 1);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.findOneByIndex(request));
    }

    @Test
    public void testList() {
        @NotNull final ProjectListRequest request = new ProjectListRequest(userToken, Sort.BY_NAME);
        @Nullable final ProjectListResponse response = projectEndpoint.listProject(request);
        Assert.assertNotNull(response);
        @Nullable final List<ProjectDTO> projects = response.getProjects();
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    public void testListTokenNull() {
        @NotNull final ProjectListRequest request = new ProjectListRequest(null, Sort.BY_NAME);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(request));
    }

    @Test
    public void testRemoveById() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(userToken, firstProjectId);
        Assert.assertNotNull(projectEndpoint.removeProjectById(request));
        @Nullable final ProjectDTO project = findProjectById(firstProjectId);
        Assert.assertNull(project);
    }

    @Test
    public void testRemoveByIdTokenNull() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(null, firstProjectId);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(request));
    }

    @Test
    public void testRemoveByIndex() {
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(userToken, 1);
        Assert.assertNotNull(projectEndpoint.removeProjectByIndex(request));
        @Nullable final ProjectDTO project = findProjectByIndex(1);
        Assert.assertNotNull(project);
        Assert.assertNotEquals(firstProjectId, project.getId());
    }

    @Test
    public void testRemoveByIndexTokenNull() {
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(null, 0);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectByIndex(request));
    }

    @Test
    public void testStartByIdTest() {
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(userToken, secondProjectId);
        Assert.assertNotNull(projectEndpoint.startProjectById(request));
        @Nullable final ProjectDTO project = findProjectById(secondProjectId);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void testStartByIdTokenNull() {
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(null, secondProjectId);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectById(request));
    }

    @Test
    public void testStartByIndex() {
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(userToken, 1);
        Assert.assertNotNull(projectEndpoint.startProjectByIndex(request));
        @Nullable final ProjectDTO project = findProjectByIndex(1);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void testStartByIndexTokenNull() {
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(null, 1);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectByIndex(request));
    }

    @Test
    public void testUpdateById() {
        @NotNull final String name = "new name";
        @NotNull final String desc = "new desc";
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(userToken, firstProjectId, name, desc);
        Assert.assertNotNull(projectEndpoint.updateProjectById(request));
        @Nullable final ProjectDTO updatedProject = findProjectById(firstProjectId);
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals(name, updatedProject.getName());
        Assert.assertEquals(desc, updatedProject.getDescription());
    }

    @Test
    public void testUpdateByIdTokenNull() {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(null, firstProjectId, "NAME", "DESC");
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(request));
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String name = "new name";
        @NotNull final String desc = "new desc";
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(userToken, 1, name, desc);
        Assert.assertNotNull(projectEndpoint.updateProjectByIndex(request));
        @Nullable final ProjectDTO updatedProject = findProjectByIndex(1);
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals(name, updatedProject.getName());
        Assert.assertEquals(desc, updatedProject.getDescription());
    }

    @Test
    public void testUpdateByIndexTokenNull() {
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(null, 0, "NAME", "DESC");
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectByIndex(request));
    }

}