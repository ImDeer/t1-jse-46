package t1.dkhrunina.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import t1.dkhrunina.tm.api.service.IConnectionService;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.api.service.model.IProjectService;
import t1.dkhrunina.tm.api.service.model.IProjectTaskService;
import t1.dkhrunina.tm.api.service.model.ITaskService;
import t1.dkhrunina.tm.api.service.model.IUserService;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.exception.entity.ProjectNotFoundException;
import t1.dkhrunina.tm.exception.entity.TaskNotFoundException;
import t1.dkhrunina.tm.exception.field.IndexIncorrectException;
import t1.dkhrunina.tm.exception.field.ProjectIdEmptyException;
import t1.dkhrunina.tm.exception.field.TaskIdEmptyException;
import t1.dkhrunina.tm.model.Project;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.service.ConnectionService;
import t1.dkhrunina.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectTaskServiceTest {

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IProjectTaskService projectTaskService;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private User user;

    @NotNull
    private User admin;

    @NotNull
    private IUserService userService;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initService() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        projectService = new ProjectService(connectionService);
        taskService = new TaskService(connectionService);
        projectTaskService = new ProjectTaskService(connectionService);
        userService = new UserService(propertyService, connectionService, projectTaskService);
        projectList = new ArrayList<>();
        projectService.clear();
        taskService.clear();
        user = userService.create("USER", "USER", "user@user.user");
        admin = userService.create("ADMIN", "ADMIN", "admin@admin.admin", Role.ADMIN);
        @NotNull final Project project1 = projectService.create(user.getId(), "project 1", "user project 1");
        @NotNull final Project project2 = projectService.create(user.getId(), "project 2", "user project 2");
        @NotNull final Project project3 = projectService.create(admin.getId(), "project 3", "admin project");
        @NotNull final Task task1 = taskService.create(user.getId(), "task 1", "project 1 task");
        task1.setProject(project1);
        taskService.update(task1);
        @NotNull final Task task2 = taskService.create(user.getId(), "task 2", "project 2 task");
        task2.setProject(project2);
        taskService.update(task2);
        @NotNull final Task task3 = taskService.create(admin.getId(), "task 3", "project 3 task");
        task3.setProject(project3);
        taskService.update(task3);
        projectList.add(project1);
        projectList.add(project2);
        projectList.add(project3);
    }

    @After
    public void afterTest() {
        taskService.clear(user.getId());
        taskService.clear(admin.getId());
        projectService.clear(user.getId());
        projectService.clear(admin.getId());
        userService.remove(user);
        userService.remove(admin);
    }

    @Test
    public void testBindTaskToProject() {
        @Nullable final List<Project> projects = projectService.findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @NotNull final Project project = projects.get(0);
        int expectedNumberOfEntries = taskService.findAllByProjectId(user.getId(), project.getId()).size() + 1;
        @NotNull final Task newTask = taskService.create(user.getId(), "Test name", "Test description");
        @NotNull final Task boundTask = projectTaskService.bindTaskToProject(user.getId(), project.getId(), newTask.getId());
        Assert.assertNotNull(boundTask);
        int newNumberOfEntries = taskService.findAllByProjectId(user.getId(), project.getId()).size();
        Assert.assertEquals(expectedNumberOfEntries, newNumberOfEntries);
    }

    @Test
    public void testBindTaskToProjectProjectIdNull() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), null, "TASK_ID"));
    }

    @Test
    public void testBindTaskToProjectProjectIdEmpty() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), "", "TASK_ID"));
    }

    @Test
    public void testBindTaskToProjectTaskIdNull() {
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), "PROJECT_ID", null));
    }

    @Test
    public void testBindTaskToProjectTaskIdEmpty() {
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), "PROJECT_ID", ""));
    }

    @Test
    public void testBindTaskToProjectProjectNotFound() {
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.bindTaskToProject(user.getId(), "PROJECT_ID", "123321"));
    }

    @Test
    public void testBindTaskToProjectTaskNotFound() {
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.bindTaskToProject(user.getId(), projectId, "123321"));
    }

    @Test
    public void testRemoveProjectById() {
        @NotNull final List<Project> projects = projectList
                .stream()
                .filter(m -> user.equals(m.getUser()))
                .collect(Collectors.toList());
        Assert.assertTrue(projects.size() > 0);
        for (@NotNull final Project project : projects) {
            projectTaskService.removeProjectById(user.getId(), project.getId());
        }
        @Nullable final List<Project> projectsAfterRemoving = projectService.findAll(user.getId());
        Assert.assertNotNull(projectsAfterRemoving);
        Assert.assertEquals(0, projectsAfterRemoving.size());
        @Nullable final List<Task> tasksAfterRemoving = taskService.findAll(user.getId());
        Assert.assertNotNull(tasksAfterRemoving);
        Assert.assertEquals(0, tasksAfterRemoving.size());
    }

    @Test
    public void testRemoveProjectByIdProjectIdNull() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(user.getId(), null));
    }

    @Test
    public void testRemoveProjectByIdProjectIdEmpty() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(user.getId(), ""));
    }

    @Test
    public void testRemoveProjectByIdProjectNotFound() {
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.removeProjectById(user.getId(), "123321"));
    }

    @Test
    public void testRemoveProjectByIndex() {
        @NotNull final List<Project> projects = projectList
                .stream()
                .filter(m -> user.equals(m.getUser()))
                .collect(Collectors.toList());
        Assert.assertTrue(projects.size() > 0);
        int index = projects.size();
        while (index > 0) {
            projectTaskService.removeProjectByIndex(user.getId(), index);
            index--;
        }
        @Nullable final List<Project> projectsAfterRemoving = projectService.findAll(user.getId());
        Assert.assertNotNull(projectsAfterRemoving);
        Assert.assertEquals(0, projectsAfterRemoving.size());
        @Nullable final List<Task> tasksAfterRemoving = taskService.findAll(user.getId());
        Assert.assertNotNull(tasksAfterRemoving);
        Assert.assertEquals(0, tasksAfterRemoving.size());
    }

    @Test
    public void testRemoveProjectByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectTaskService.removeProjectByIndex(user.getId(), null));
    }

    @Test
    public void testRemoveProjectByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectTaskService.removeProjectByIndex(user.getId(), -1));
    }

    @Test
    public void testRemoveProjectByIndexInvalid() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectTaskService.removeProjectByIndex(user.getId(), projectList.size() + 1));
    }

    @Test
    public void testClearProject() {
        int numberOfProjects = projectService.getSize(user.getId());
        Assert.assertTrue(numberOfProjects > 0);
        int numberOfTasks = taskService.getSize(user.getId());
        Assert.assertTrue(numberOfTasks > 0);
        projectTaskService.removeAllByUserId(user.getId());
        int numberOfProjectsAfterRemoving = projectService.getSize(user.getId());
        Assert.assertEquals(0, numberOfProjectsAfterRemoving);
        int numberOfTasksAfterRemoving = taskService.getSize(user.getId());
        Assert.assertEquals(0, numberOfTasksAfterRemoving);
    }

    @Test
    public void testUnbindTaskFromProject() {
        @Nullable final List<Project> projects = projectService.findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @NotNull final Project project = projects.get(0);
        @Nullable final List<Task> tasks = taskService.findAllByProjectId(user.getId(), project.getId());
        Assert.assertNotNull(tasks);
        @NotNull final String taskForUnbindId = tasks.get(0).getId();
        @Nullable final Task boundTask = projectTaskService.bindTaskToProject(user.getId(), project.getId(), taskForUnbindId);
        Assert.assertNotNull(boundTask);
        @Nullable Task unboundTask = projectTaskService.unbindTaskFromProject(user.getId(), boundTask.getProject().getId(), boundTask.getId());
        Assert.assertNotNull(unboundTask);
        unboundTask = taskService.findOneById(user.getId(), unboundTask.getId());
        Assert.assertNotNull(unboundTask);
        Assert.assertNull(unboundTask.getProject());
    }

    @Test
    public void testUnbindTaskProjectIdNull() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), null, "TASK_ID"));
    }

    @Test
    public void testUnbindTaskProjectIdEmpty() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), "", "TASK_ID"));
    }

    @Test
    public void testUnbindTaskTaskIdNull() {
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), "PROJECT_ID", null));
    }

    @Test
    public void testUnbindTaskTaskIdEmpty() {
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), "PROJECT_ID", ""));
    }

    @Test
    public void testUnbindTaskProjectNotFound() {
        Assert.assertThrows(ProjectNotFoundException.class, ()->projectTaskService.unbindTaskFromProject(user.getId(), "PROJECT_ID", "123321"));
    }

    @Test
    public void testUnbindTaskTaskNotFound() {
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertThrows(TaskNotFoundException.class, ()->projectTaskService.unbindTaskFromProject(user.getId(), projectId, "123321"));
    }

}